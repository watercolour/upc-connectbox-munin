#!/usr/bin/env python3

import argparse
import pprint
import os
import sys
from lxml import etree
from io import StringIO, BytesIO

# Ide tessek megadni az utvonalat, ahol a compal_CH7465LG_py van.
sys.path.append('/path/to/compal_CH7465LG_py')

from compal import Compal

def upstream_power(host, passwd):
    modem = Compal(host, passwd)
    modem.login()
    resp = modem.xml_getter(11,{})
    modem.logout()
    root = etree.XML(bytes(bytearray(resp.text, encoding='utf-8')))
    for upstream in root:
        if upstream.tag == 'upstream':
            cid = upstream.find('usid').text
            cpow = upstream.find('power').text
            print('puc{}.value {}'.format(cid,cpow))

def downstream_snr(host,passwd):
    modem = Compal(host, passwd)
    modem.login()
    resp = modem.xml_getter(10,{})
    modem.logout()
    root = etree.XML(bytes(bytearray(resp.text, encoding='utf-8')))
    for downstream in root:
        if downstream.tag == 'downstream':
            cid = downstream.find('chid').text
            csnr = downstream.find('snr').text
            print('pds{}.value {}'.format(cid,csnr))

def downstream_power(host,passwd):
    modem = Compal(host, passwd)
    modem.login()
    resp = modem.xml_getter(10,{})
    modem.logout()
    root = etree.XML(bytes(bytearray(resp.text, encoding='utf-8')))
    for downstream in root:
        if downstream.tag == 'downstream':
            cid = downstream.find('chid').text
            cpow = downstream.find('pow').text
            print('pdp{}.value {}'.format(cid,cpow))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Connect Box configuration')
    parser.add_argument('--host', type=str,
                        default=os.environ.get('CB_HOST', None))
    parser.add_argument('--password', type=str,
                        default=os.environ.get('CB_PASSWD', None))
    parser.add_argument('--data', type=str,
                        default=os.environ.get('CB_DATA', None))

    args = parser.parse_args()

    if args.data == 'dsnr':
        downstream_snr(args.host, args.password)
    elif args.data == 'dpow':
        downstream_power(args.host, args.password)
    else:
        upstream_power(args.host, args.password)


