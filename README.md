# upc-connectbox-munin
Először is, szükséges egy telepített, működő munin. Erre sok leírást találhatsz az interneten.
A leírás Ubuntu 16.04-et feltételez. Más rendszeren is működik, viszont magadnak kell feltelepíteni.

1. `git clone https://bitbucket.org/watercolour/upc-connectbox-munin.git`
2. `sudo apt-get install libxml2 libxml2-dev python3-pip python3-lxml python3-setuptools python3-dev`
3. `sudo pip3.5 install recordclass requests`
4. `git clone https://github.com/ties/compal_CH7465LG_py.git`
5. `upc_*` fájlok elejében a paramétereket (IP cím, csatornák száma, jelszó, cb-data.py abszolút útvonala) kitölteni a megfelelő értékekkel
6. `cb-data.py`: `sys.path.append` kezdetű sorba megadni a `compal_CH7465LG_py` útvonalát
7. `sudo cp upc_* /etc/munin/plugins/`
8. `sudo systemctl restart munin-node`
